    <div class="loader">
        <div class="loadercircle"></div> 
    </div>
    <div class="row">
        <input type="hidden" class="titleval" />
        <input type="hidden" class="scoreval" /> 
        <div class="col-md-auto"> 
            <ul class="mood">
                <li data-val="Happy" data-catid="14">
                    <a href="javascript:">
                        <img src="<?php echo get_template_directory_uri(); ?>/common/images/happy.png" alt="img"> 
                        <!-- <span>Happy</span> -->
                    </a>
                </li>
                <li data-val="Neutral" data-catid="15">
                    <a href="javascript:">
                        <img src="<?php echo get_template_directory_uri(); ?>/common/images/neutral.png" alt="img"> 
                        <!-- <span>Neutral</span> -->
                    </a>
                </li>
                <li data-val="Angry" data-catid="16">
                    <a href="javascript:">
                        <img src="<?php echo get_template_directory_uri(); ?>/common/images/angry.png" alt="img"> 
                        <!-- <span>Angry</span> -->
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md">
            <div class="moodsubmit">
                <p class="message">Successfully Submitted</p>
                <input type="text" name="" placeholder="Comment / feedback..." class="textfield commentval">
                <input type="submit" value="Submit" class="btnsubmit">
            </div>
        </div>
    </div>