<?php

/**
 * Template Name: Thank You
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



        <section class="content thankspage greybg">
            <div class="container">
                <div class="mehroonwbx">
                    <div class="row no-gutters align-items-center position-relative">

                        <div class="col-12 col-md-6">
                            <div class="whitebg thanksform">
                                <!-- <h2>Your Idea</h2> -->
                                <div class="formbx">
                                    <form>
                                        <div class="form-group">
                                            <label class="helpf">Thank you for ....</label>
                                            <textarea name="" class="helptext"></textarea>
                                        </div>
                                        <div class="form-group pb60">  
                                            <div class="row">
                                                <div class="checkbox col-6">
                                                    <input type="checkbox" name="phone" value="phone" id="ddd">
                                                    <label for="ddd"> Person</label>
                                                </div>
                                                <div class="checkbox col-6">
                                                    <input type="checkbox" name="phone2" value="phone2" id="aaa">
                                                    <label for="aaa">ILT</label>
                                                </div>
                                                <div class="checkbox col-6">
                                                    <input type="checkbox" name="phone3" value="phone2" id="aaa1">
                                                    <label for="aaa1">Initiative</label>
                                                </div>
                                                <div class="checkbox col-6">
                                                    <input type="checkbox" name="phone4" value="phone2" id="aaa2">
                                                    <label class="pt-0" for="aaa2">Business Functions</label>
                                                </div>
                                                <div class="checkbox col-6">
                                                    <input type="checkbox" name="phone5" value="phone2" id="aaa3">
                                                    <label for="aaa3">IMO Streams</label>
                                                </div>
                                                <div class="checkbox col-6">
                                                    <input type="checkbox" name="phone6" value="phone2" id="aaa4">
                                                    <label for="aaa4">Others</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row align-items-center">
                                                <div class="col-auto"><label for="usr">Name:</label></div>
                                                <div class="col"><input type="text" class="form-control" id="usr" name="username"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row align-items-center">
                                                <div class="col-auto"><label for="usr">Email:</label></div>
                                                <div class="col"><input type="email" class="form-control" id="eml" name="eml"></div>
                                            </div>
                                        </div>
                                        <div class="btnbx text-right"><input type="submit" value="Submit" class="btnsubmit-yellow"></div>
                                    </form>
                                </div>
                                <!-- <a href="<?php echo get_option('home') ?>/start/" class="back-idea"><img src="<?php echo get_template_directory_uri(); ?>/common/images/back.jpg" alt="img"></a> -->
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="thanksbx">
                                <img src="<?php echo get_template_directory_uri(); ?>/common/images/thanks.png" alt="img" class="thnks">
                                <p>Give thanks for initiatives that you support or give credit to your colleagues for initiatives taken or for their hard work! </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>



        <?php

    endwhile;
endif;
?>       


<?php get_footer(); ?>