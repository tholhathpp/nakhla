<?php
/**
 * Template Name: Your Idea
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



        <section class="content youridea greybg">
            <div class="container">
                <div class="yellowbx">
                    <div class="row no-gutters align-items-center position-relative">

                        <div class="col-12 col-md-6">
                            <div class="bulbbx">
                                <img src="<?php echo get_template_directory_uri(); ?>/common/images/bulb.png" alt="img" class="bulb">
                                <h2>New Ideas</h2>
                                <p>Have a brilliant idea? Why don't  you share it with Nakhla? <br> Offer your opinion. Get involved!</p>
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="whitebg ideaform">
                                <h2>Your Idea</h2>
                                <div class="formbx"> 
                                    
                                    <div class="loader">
                                        <div class="loadercircle"></div> 
                                    </div>
                                    
                                    <div class="form-group">
                                        <textarea name="useridea" id="useridea" class="ideatext"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="row align-items-center">
                                            <div class="col-auto"><label for="usr">Name:</label></div>
                                            <div class="col">
                                                <input type="text" class="form-control" id="username" name="username">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row align-items-center">
                                            <div class="col-auto"><label for="usr">Email:</label></div>
                                            <div class="col">
                                                <input type="email" class="form-control" id="useremail" name="useremail">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btnbx text-right">
                                        <input type="submit" value="Submit" class="btnsubmit-yellow submitidea">
                                        <p class="message">Successfully Submitted</p> 
                                    </div> 
                                    
                                </div>
<!--                                <a href="<?php echo get_option('home') ?>/start/" class="back-idea">
                                    <img src="<?php echo get_template_directory_uri(); ?>/common/images/back.jpg" alt="img">
                                </a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <?php
    endwhile;
endif;
?>       

<script>
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
 

    $(".submitidea").on('click', (function (e) {
        e.preventDefault();

        var useridea = $('#useridea').val();
        var username = $('#username').val();
        var useremail = $('#useremail').val();



        var i = true;
        if (useridea == "") {
            $('#useridea').addClass('error');
            i = false;
        } else {
            $('#useridea').removeClass('error');
        }
        if (username == "") {
            $('#username').addClass('error');
            i = false;
        } else {
            $('#username').removeClass('error');
        }
        if (useremail == "") {
            $('#useremail').addClass('error');
            i = false;
        } else {
            $('#useremail').removeClass('error');
        }

        if (isEmail(useremail) == false) {
            $('#useremail').addClass('error');
            i = false;
        } else {
            $('#useremail').removeClass('error');
        }

        if (i == false) {
            $('.message').hide();
            return false;
        }



        var url = "<?php echo admin_url(); ?>admin-ajax.php?action=insertIdeas&useridea="+useridea+"&username=" + username + "&useremail=" + useremail;
        jQuery.ajax({
            type: "POST",
            url: url,
            beforeSend: function (arr, $form, options) {
                $('.loader').show();
            },
            success: function (result) {
                $('.loader').hide();
                $('.message').show();

                $('#username').val('');
                $('#useremail').val('');
                $('#useridea').val('');
            }});

    }));
</script>

<?php get_footer(); ?>