<?php
get_header();
?>
<?php
$innerbanner = get_post_meta(16, 'innerbanner', TRUE);
?>
<div class="innerbanner" style="background:url(<?php echo $innerbanner; ?>) no-repeat center center;"></div>



<section class="main-container">
             <div class="content clearfix" style="margin-top:0;">
<?php
if (have_posts()) : while (have_posts()) : the_post();
        ?> 

      
            <div class="search-results">
                <a href="<?php echo the_permalink(); ?>">
                    <h1><?php the_title(); ?></h1>
                </a>
                <p> <?php
                the_excerpt();
                ?></p>
            </div>
     

        <?php
    endwhile;
    if (function_exists("pagination")) {
        pagination();
    }
    ?>
<?php else : ?>
  
<div class="not-found-copy">
                        <h2 class="center">Not Found</h2>
                        <p class="center">Sorry, but you are looking for something that isn't here.</p>
                        <?php include (TEMPLATEPATH . "/searchform.php"); ?>
</div>
            
<?php endif; ?> 
</div>
               
            </section>



<?php get_footer(); ?>