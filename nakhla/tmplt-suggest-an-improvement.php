<?php
/**
 * Template Name: Suggest an improvement
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



        <section class="content suggestion greybg">
            <div class="container">
                <div class="whitebg">
                    <div class="row no-gutters align-items-center position-relative">
                        <div class="col-12 col-md-6">
                            <div class="greenbx">
                                <div class="faq-contaner">
                                    <div class="faqbx">
                                        <h3><span data-val="Imo" data-catid="2">1. IMO</span></h3>
                                        <div class="faqcontent">
                                        <?php include('improvefaq.php'); ?>
                                    </div>
                                    </div>
                                    <div class="faqbx">
                                        <h3><span data-val="Finance and Strategy" data-catid="3">2. Finance &amp; Strategy</span></h3>
                                        <div class="faqcontent">
                                        <?php include('improvefaq.php'); ?>
                                    </div>
                                    </div>
                                    <div class="faqbx">
                                        <h3><span data-val="People and Culture" data-catid="4">3. People &amp; Culture</span></h3>
                                        <div class="faqcontent">
                                        <?php include('improvefaq.php'); ?>
                                    </div>
                                    </div>
                                    <div class="faqbx">
                                        <h3><span data-val="Blueprint F and S" data-catid="5">4. Blueprint F&amp;S</span></h3>
                                        <div class="faqcontent">
                                        <?php include('improvefaq.php'); ?>
                                    </div>
                                    </div>
                                    <div class="faqbx">
                                        <h3><span data-val="Blueprint UpStream" data-catid="6">5. Blueprint UpStream</span></h3>
                                        <div class="faqcontent">
                                        <?php include('improvefaq.php'); ?>
                                    </div>
                                    </div>
                                    <div class="faqbx">
                                        <h3><span data-val="Engagement" data-catid="7">6. Engagement</span></h3>
                                        <div class="faqcontent">
                                        <?php include('improvefaq.php'); ?>
                                    </div>
                                    </div>
                                    <div class="faqbx">
                                        <h3><span data-val="Quick Win" data-catid="8">7. Quick Win</span></h3>
                                        <div class="faqcontent">
                                        <?php include('improvefaq.php'); ?>
                                    </div>
                                    </div>
                                    <div class="faqbx">
                                        <h3><span data-val="ERP" data-catid="9">8. ERP</span></h3>
                                        <div class="faqcontent">
                                        <?php include('improvefaq.php'); ?>
                                    </div>
                                    </div>
                                    <div class="faqbx">
                                        <h3><span data-val="QHSSE" data-catid="10">9. QHSSE</span></h3>
                                        <div class="faqcontent">
                                        <p>Our team is in charge of building our new Group’s framework and culture around Quality, Health, Safety, Security, and Environment (“QHSSE”). We are promoting a safety culture where each person ensures their own safety and the safety of all those around them. We are also identifying advanced solutions to create this environment efficiently. Along the way, we are identifying opportunities to optimize QHSSE through ‘Quick Wins’.</p>
                                        <?php include('improvefaq.php'); ?>
                                    </div>
                                    </div>
                                    <div class="faqbx">
                                        <h3><span data-val="Lean" data-catid="11">10. Lean</span></h3>
                                        <div class="faqcontent">
                                        <?php include('improvefaq.php'); ?>
                                    </div>
                                    </div>
                                    <div class="faqbx">
                                        <h3><span data-val="Digitalization" data-catid="12">11. Digitalization</span></h3>
                                        <div class="faqcontent">
                                        <?php include('improvefaq.php'); ?>
                                    </div>
                                    </div>
                                    <div class="faqbx">
                                        <h3><span data-val="Ambience" data-catid="13">12. Ambience</span></h3>
                                        <div class="faqcontent">
                                        <?php include('improvefaq.php'); ?>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="improvement">
                                <img src="<?php echo get_template_directory_uri(); ?>/common/images/green-circle.png" alt="img" class="greencircle">
                                <h2>Area for <br> improvement</h2>
<!--                                <div class="knowmore">To know more about each stream's <br> descriptions please click 
                                    <a href="<?php echo get_option('home'); ?>/join/">here</a>.</div>-->
                            </div>
                        </div>
                        <a href="<?php echo get_option('home'); ?>/start/" class="back-right">
                            <img src="<?php echo get_template_directory_uri(); ?>/common/images/back.jpg" alt="img">
                        </a>
                    </div>
                </div>
            </div>
        </section>



        <?php
    endwhile;
endif;
?>       

<script>

    $(".faqbx h3").on('click', (function (e) {
        var spanval = $(this).find('span').data('val');
        $(this).parent('.faqbx').find('.titleval').val(spanval);
    }));

    $(".faqbx .faqcontent .mood li").on('click', (function (e) {
        var scoreval = $(this).data('val');
        $(this).parent('.mood').find('li a').removeClass('active');
        $(this).find('a').addClass('active');

        $(this).parents('.faqcontent').find('.scoreval').val(scoreval);
    }));


    $(".btnsubmit").on('click', (function (e) {
        e.preventDefault();

        var current = $(this).parents('.faqcontent');
        var curfaq = $(this).parents('.faqbx');
        var title = current.find('.titleval').val();
        var score = current.find('.scoreval').val();
        var comment = current.find('.commentval').val();

        var catid = $(this).parents('.faqbx').find('h3 span').data('catid');
        var scorecatid = $(this).parents('.faqbx').find('.mood li a.active').parent('li').data('catid');


        var i = true;
        if (comment == "") {
            current.find('.commentval').addClass('error');
            i = false;
        } else {
            current.find('.commentval').removeClass('error');
        }
        if (score == "") {
            $(this).parents('.faqbx').find('.mood').addClass('error');
            i = false;
        } else {
            $(this).parents('.faqbx').find('.mood').removeClass('error');
        }
        if (i == false) {
            current.find('.message').hide();
            return false;
        }



        var url = "<?php echo admin_url(); ?>admin-ajax.php?action=insertImprovementarea&title=" + title + "&score=" + score + "&comment="
                + comment + "&catid=" + catid + "&scorecatid=" + scorecatid;
        jQuery.ajax({
            type: "POST",
            url: url,
            beforeSend: function (arr, $form, options) {
                current.find('.loader').show();
            },
            success: function (result) {
                current.find('.loader').hide();
                current.find('.message').show();
                current.find('.commentval').val('');
                curfaq.find('.mood li a').removeClass('active');
                current.find('.scoreval').val('');
            }});

    }));
</script>


<?php get_footer(); ?>