<?php
 
get_header();
 
?>
 
        <?php
        $innerbanner = get_post_meta(16, 'innerbanner', TRUE);
        ?>
        <div class="innerbanner" style="background:url(<?php echo get_template_directory_uri(); ?>/images/404.jpg) no-repeat center center;"></div>
        <section class="main-container">
            <div class="content clearfix">
                 
              
                <div class="not-found-copy">
                    <h2 class="center">Try something else</h2>
                        <?php include (TEMPLATEPATH . "/searchform.php"); ?>
                </div>
               
            </div>
        </section>




 


<?php get_footer(); ?>

 