// JavaScript Document
$(document).ready(function () {

    var winw = $(window).width();
    var winh = $(window).height();


    //Cumstom Menu For Mobile
    $(".menu-show").click(function () {
        $(this).toggleClass("active");
        $(".menuconainer").toggleClass("active");
    });
    $(".closeMenu").click(function () {
        $(".menu-show, .menuconainer").removeClass("active");
    });
	

    //Cumstom Menu For Mobile
    if (winw < 991) {
        $(".menubx li.menu-item-has-children").prepend("<i class='sidr-dropdown-toggler'></i>");

        $(".menubx li.menu-item-has-children > i").click(function () {
            $(this).parent().find(".sub-menu").slideToggle();
            $(this).parent().toggleClass("nav-item-open");
        });
        $(".menubx li.menu-item-depth-1 > i").click(function () {
            $(this).parent().find(".sub-sub-menu").slideToggle();
            $(this).parent().toggleClass("sub-nav-item-open");
        });
    }

    new WOW().init();
	
	// Faq page and other page accordian	
	$('.faqbx h3').click(function(){
		$(this).parent().siblings('.faqbx').find('.faqcontent').slideUp('slow');
		$(this).parent().siblings('.faqbx').find('h3').removeClass('active');
		$(this).next('.faqcontent').slideToggle('slow');
		$(this).toggleClass('active');
	});

    /*Hack For IOS*/
    if (navigator.userAgent.indexOf('Mac OS X') != -1) {
        $("body").addClass("ios");
    } else {
    }

    //# Link Scrolling Animation
    $('a[href^="#"]:not([href="#"]):not([data-toggle])').on('click', function (e) {
        e.preventDefault();
        var target = this.hash;
        var $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 1300, 'swing', function () {
            window.location.hash = target;
        });
    });

    $('.modal.video').on('hidden.bs.modal', function () {
        closeVedios();
    });

    // function closeVedios() {
    //     var video = $(".videoplayer iframe").attr("src");
    //     $(".videoplayer iframe").attr("src", "");
    //     $(".videoplayer iframe").attr("src", video);
    // }

    // $(".videoplay").click(function () {
    //     var videourl = $(this).data('src');
    //     $(".videoplayer iframe").attr("src", videourl); 
    // });


});

