
<div class="row">
    <div class="col-6 col-md-3">
        <div class="box">
            <a href="<?php echo get_option('home'); ?>/suggest-an-improvement/">
                <img src="<?php echo get_template_directory_uri(); ?>/common/images/menu1.png" alt="img">
                <h3>Suggest an <br> Improvement</h3>
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="box">
            <a href="<?php echo get_option('home'); ?>/join">
                <img src="<?php echo get_template_directory_uri(); ?>/common/images/menu2.png" alt="img">
                <h3>Join an <br> Nakhla Stream</h3>
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="box">
            <a href="<?php echo get_option('home'); ?>/your-idea">
                <img src="<?php echo get_template_directory_uri(); ?>/common/images/menu3.png" alt="img">
                <h3>Give a New Idea</h3>
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="box">
            <a href="<?php echo get_option('home'); ?>/thank-you">
                <img src="<?php echo get_template_directory_uri(); ?>/common/images/menu4.png" alt="img">
                <h3>Thank <br> Someone :)</h3>
            </a>
        </div>
    </div>
</div>