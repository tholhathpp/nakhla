<?php

/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Orpic
 */
//get_header(); 
global $wp_query;

$wp_query->set_404();
status_header(404);
get_template_part(404);
exit();
?>



<?php

get_sidebar();
get_footer();
