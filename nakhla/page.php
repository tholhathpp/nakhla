<?php
get_header();
wp_enqueue_script('jquery');
add_action('wp_footer', 'endeavor_pagescipts', 21);

function endeavor_pagescipts() {
    
}
?>



<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



        <section>
            <div class="content clearfix">
                <h1 class="heading"><?php the_title(); ?></h1>
                <div class="container-1">
                    <?php the_content(); ?>
                </div>
            </div>
        </section>




        <?php
    endwhile;
endif;
?>       


<?php get_footer(); ?>