<?php
/**
 * Template Name: Team
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



        <section class="content jointeam greybg">
            <div class="container">
                <div class="whitebg">
                    <div class="row">



                        <?php
                        $args = array(
                            'post_type' => 'team',
                            'posts_per_page' => -1,
                            'post_status' => 'publish',
                            'order' => 'DESC'
                        );
                        $loop = new WP_Query($args);
                        while ($loop->have_posts()) : $loop->the_post();
                            ?>
                            <div class="col-md-2 member">
                                <div class="box">
                                    <a href="javascript:" data-toggle="modal" data-target="#vedioModal" class="popupelement">
                                        <?php
                                        $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
                                        $main_img_url = get_field('main_image');
                                        ?>
                                        <img src="<?php echo $featured_img_url; ?>" alt="<?php the_title(); ?>">
                                        <h2><?php echo get_field('secondary_title'); ?></h2>


                                        <div style="display: none;">
                                            <div id="teamcontent"><?php the_content(); ?></div>
                                            <input type="hidden" id="teamid" value="<?php echo get_the_ID(); ?>"/>
                                            <input type="hidden" id="teamimage" value="<?php echo $main_img_url; ?>"/>
                                            <input type="hidden" id="teamtitle" value="<?php echo get_field('secondary_title'); ?>"/>
                                        </div>


                                    </a>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        ?> 











                    </div>
                </div>
            </div>
        </section>
        <!--Menu End-->














        <!-- The Modal-->
        <div class="modal fade memberpop" id="vedioModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="container no-padding">
                            <div class="row no-gutters">
                                <div class="col-md-6">
                                    <div class="memberdetail">

                                        <input type="hidden" id="popid" />
                                        <div id="popcontent">***</div>

                                        <div class="formbx"> 

                                            <div class="loader">
                                                <div class="loadercircle"></div> 
                                            </div>

                                            <div class="form-group">
                                                <div class="row align-items-center">
                                                    <div class="col-auto"><label for="usr">Name:</label></div>
                                                    <div class="col">
                                                        <input type="text" class="form-control" id="username" name="username">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row align-items-center">
                                                    <div class="col-auto"><label for="usr">Email:</label></div>
                                                    <div class="col">
                                                        <input type="email" class="form-control" id="useremail" name="useremail">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="btnbx">
                                                <button type="submit" class="btn joinbtn">Join The Team</button>

                                                <p class="message">Successfully Submitted</p>
                                            </div>

                                        </div>
                                        <a href="javascript:" data-dismiss="modal" class="back-left">
                                            <img src="<?php echo get_template_directory_uri(); ?>/common/images/back.jpg" alt="img">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="box">
                                        <img src="***" alt="img" id="popimage">
                                        <h2 id="poptitle">***</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- The Modal-->




        <?php
    endwhile;
endif;
?>       


<script>
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    $(".popupelement").on('click', (function (e) {

        $('.message').hide();
        $('#username').val('');
        $('#useremail').val('');

        var teamid = $(this).find('#teamid').val();
        var teamcontent = $(this).find('#teamcontent').html();
        var teamimage = $(this).find('#teamimage').val();
        var teamtitle = $(this).find('#teamtitle').val();


        $('#popid').val(teamid);
        $('#popcontent').html(teamcontent);
        $("#popimage").attr("src", teamimage);
        $("#poptitle").html(teamtitle);

    }));



    $(".joinbtn").on('click', (function (e) {
        e.preventDefault();

        var postid = $('#popid').val();
        var username = $('#username').val();
        var useremail = $('#useremail').val();



        var i = true;
        if (username == "") {
            $('#username').addClass('error');
            i = false;
        } else {
            $('#username').removeClass('error');
        }
        if (useremail == "") {
            $('#useremail').addClass('error');
            i = false;
        } else {
            $('#useremail').removeClass('error');
        }

        if (isEmail(useremail) == false) {
            $('#useremail').addClass('error');
            i = false;
        } else {
            $('#useremail').removeClass('error');
        }

        if (i == false) {
            $('.message').hide();
            return false;
        }



        var url = "<?php echo admin_url(); ?>admin-ajax.php?action=insertTeamData&username=" + username + "&useremail=" + useremail
                + "&postid=" + postid;
        jQuery.ajax({
            type: "POST",
            url: url,
            beforeSend: function (arr, $form, options) {
                $('.loader').show();
            },
            success: function (result) {
                $('.loader').hide();
                $('.message').show();

                $('#username').val('');
                $('#useremail').val('');
            }});

    }));
</script>


<?php get_footer(); ?>