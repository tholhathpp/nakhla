<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />

        <?php
        if (isset($_SERVER['HTTP_USER_AGENT']) &&
                (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))
            @header('X-UA-Compatible: IE=edge,chrome=1');
        ?>

        <title><?php bloginfo('name'); ?> <?php wp_title('|', true, 'left'); ?></title>
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/common/images/ico.png" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="<?php echo get_template_directory_uri(); ?>/common/css/animate.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/common/css/default.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/common/css/media.css" rel="stylesheet">
        <!--<link href="common/css/bootstrap.min.css" rel="stylesheet">-->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--<script type="text/javascript" src="common/js/jquery.min.js"></script>
        <script type="text/javascript" src="common/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="common/js/wow.min.js"></script>
        <script type="text/javascript" src="common/js/bootstrapValidator.js"></script>-->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/common/js/default.js"></script>



        <script type="text/javascript">
            var get_template_directory_uri = '<?php echo get_template_directory_uri(); ?>';
        </script>

        <?php
        wp_head();
        ?>

    </head>

    <body <?php body_class(); ?>>

        <header>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-auto">
                        <a href="<?php echo get_option('home'); ?>/" class="logo-left">
                            <img src="<?php echo get_template_directory_uri(); ?>/common/images/feedna.png" title="Home" alt="img">
                        </a>
                    </div>
                    <div class="col">
                        <div class="menubar topmenu">
                            <?php include('menu.php'); ?>
                        </div>
                    </div>
                    <div class="col-auto">
                        <a href="<?php echo get_option('home'); ?>/" class="logo">
                            <img src="<?php echo get_template_directory_uri(); ?>/common/images/logo.png" title="Home" alt="img">
                        </a>
                    </div>
                </div>
            </div>
        </header>

        <!--==============================content================================-->