<?php

add_action('login_enqueue_scripts', 'acd_login_enqueue_scripts');
add_filter('login_headerurl', 'get_home_url');

function acd_login_enqueue_scripts() {
    ?>
    <style type="text/css">
    </style>
    <?php

}

function acd_scripts_styles() {
    
}

add_action('wp_enqueue_scripts', 'acd_scripts_styles');

/* * ***********header image***************** */
$args = array(
    'default-image' => get_template_directory_uri() . '/images/header.jpg',
    'uploads' => true,
    'header-text' => false,
);
add_theme_support('custom-header', $args);
/* * ***********header image***************** */



//***********this is for menu navigation registration*************************
register_nav_menus(array(
    'top_menu' => 'Top Most Menu',
    'foot_menu' => 'Footer Menu '
));
add_theme_support('post-thumbnails');
set_post_thumbnail_size(250, 250);
//add_image_size('modelimg', 235, 331, true); 


/* * **************post type Testimonials********************** */
add_action('init', 'pro_post_type');

function pro_post_type() {
    register_post_type('improvementarea', array(
        'labels' => array(
            'name' => __('Improvement Area'),
            'singular_name' => __('Improvement Area'),
            'add_new' => 'Add New Area',
            'add_new_item' => 'Add New Area',
            'edit_item' => 'Edit Area',
            'new_item' => 'New Area',
            'all_items' => 'All Our Areas',
            'view_item' => 'View Our Areas',
            'search_items' => 'Search Our Areas',
            'not_found' => 'No Area found',
            'not_found_in_trash' => 'No Area found in Trash',
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'improvementarea'),
        'supports' => array('title', 'editor', 'thumbnail'),
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
            )
    );
    register_post_type('team', array(
        'labels' => array(
            'name' => __('Team'),
            'singular_name' => __('Team'),
            'add_new' => 'Add New Team',
            'add_new_item' => 'Add New Team',
            'edit_item' => 'Edit Team',
            'new_item' => 'New Team',
            'all_items' => 'All Our Teams',
            'view_item' => 'View Our Teams',
            'search_items' => 'Search Our Teams',
            'not_found' => 'No Team found',
            'not_found_in_trash' => 'No Team found in Trash',
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'team'),
        'supports' => array('title', 'editor', 'thumbnail'),
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
            )
    );
    register_post_type('ideas', array(
        'labels' => array(
            'name' => __('Your Ideas'),
            'singular_name' => __('Idea'),
            'add_new' => 'Add New Idea',
            'add_new_item' => 'Add New Idea',
            'edit_item' => 'Edit Idea',
            'new_item' => 'New Idea',
            'all_items' => 'All Our Ideas',
            'view_item' => 'View Our Ideas',
            'search_items' => 'Search Our Ideas',
            'not_found' => 'No Idea found',
            'not_found_in_trash' => 'No Idea found in Trash',
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'ideas'),
        'supports' => array('title', 'editor', 'thumbnail'),
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
            )
    );
}

// create taxonomies
add_action('init', 'faqcat', 0);

function faqcat() {
    $labels = array(
        'name' => _x('Area Categories', 'taxonomy general name'),
        'singular_name' => _x('Area Category', 'taxonomy singular name'),
        'search_items' => __('Search Area Categories'),
        'all_items' => __('All Area Categories'),
        'parent_item' => __('Parent Area Category'),
        'parent_item_colon' => __('Parent Area Category:'),
        'edit_item' => __('Edit Area Category'),
        'update_item' => __('Update Area Category'),
        'add_new_item' => __('Add New Area Category'),
        'new_item_name' => __('New Area Category Name'),
        'menu_name' => __('Area Categories'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'faqcat'),
    );
    register_taxonomy('faqcat', array('improvementarea'), $args);
}

add_action('init', 'scorecat', 0);

function scorecat() {
    $labels = array(
        'name' => _x('Score Categories', 'taxonomy general name'),
        'singular_name' => _x('Score Category', 'taxonomy singular name'),
        'search_items' => __('Search Score Categories'),
        'all_items' => __('All Score Categories'),
        'parent_item' => __('Parent Score Category'),
        'parent_item_colon' => __('Parent Score Category:'),
        'edit_item' => __('Edit Score Category'),
        'update_item' => __('Update Score Category'),
        'add_new_item' => __('Add New Score Category'),
        'new_item_name' => __('New Score Category Name'),
        'menu_name' => __('Score Categories'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'scorecat'),
    );
    register_taxonomy('scorecat', array('improvementarea'), $args);
}

//------------ Widget Section named 'Footer Sidebar' starts-----
function argate_widgets_init() {
    register_sidebar(array(
        'name' => __('Footer Sidebar', 'savo'),
        'id' => 'sidebar-1',
        'description' => __('', 'argatefooter'),
        'before_widget' => '<section id="%1$s" class="widget %2$s clearfix">',
        'after_widget' => '</section>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
}

add_action('widgets_init', 'argate_widgets_init');

//-------------------------


add_action('wp_ajax_nopriv_insertImprovementarea', 'insertImprovementarea');
add_action('wp_ajax_insertImprovementarea', 'insertImprovementarea');

function insertImprovementarea() {
    $date = date('Y-m-d H:i:s');
    $title = $_REQUEST['title'];
    $score = $_REQUEST['score'];
    $comment = $_REQUEST['comment'];
    $faqcatid = $_REQUEST['catid'];
    $scorecatid = $_REQUEST['scorecatid'];

    $post_id = wp_insert_post(array(
        'post_type' => 'improvementarea',
        'post_title' => $title,
        'post_content' => $comment,
        'post_status' => 'publish',
        'comment_status' => 'closed',
        'ping_status' => 'closed',
    ));

    if ($post_id) {
        wp_set_post_terms($post_id, $faqcatid, 'faqcat');
        wp_set_post_terms($post_id, $scorecatid, 'scorecat');
    }

    echo 1;
}

//-------insertTeamData
add_action('wp_ajax_nopriv_insertTeamData', 'insertTeamData');
add_action('wp_ajax_insertTeamData', 'insertTeamData');

function insertTeamData() {
    $username = $_REQUEST['username'];
    $useremail = $_REQUEST['useremail'];
    $postid = $_REQUEST['postid'];

    $values = array($username, $useremail);

    if ($postid) {
        $field_key = "field_5d8b48143e731";

        $value = get_field($field_key, $postid);
        $value[] = array("name" => $username, "email" => $useremail);

        update_field($field_key, $value, $postid);
    }

    echo 1;
}

//-------insertIdeas
add_action('wp_ajax_nopriv_insertIdeas', 'insertIdeas');
add_action('wp_ajax_insertIdeas', 'insertIdeas');

function insertIdeas() {
    $useridea = $_REQUEST['useridea'];
    $username = $_REQUEST['username'];
    $useremail = $_REQUEST['useremail'];

    $post_id = wp_insert_post(array(
        'post_type' => 'ideas',
        'post_title' => $username,
        'post_content' => $useridea,
        'post_status' => 'publish',
        'comment_status' => 'closed',
        'ping_status' => 'closed',
    ));

    if ($post_id) {
        add_post_meta($post_id, 'email', $useremail);
    }

    echo 1;
}
?>