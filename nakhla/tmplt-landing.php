<?php

/**
 * Template Name: Landing Page
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



        <section class="slider">
            <img src="<?php echo get_template_directory_uri(); ?>/common/images/cover2.jpg" alt="img">
        </section>
        <!--Slider End-->

        <!--Menu Start-->
        <section class="content landingbx">
            <div class="container">
                <p><span>IMO Streams</span> are hard at work developing and testing ways for us to <span>work better,</span> be more <span>efficient</span>, be <span>innovative</span> and drive <span>engagement</span> on our collective <span>goals.</span></p>
                <p class="green">Offer your opinions and feedback on ways to improve.</p>
                <p class="green">Give thanks to initiatives and teams.</p>
                <p class="green">Together we make a better future. <span>Get involved!</span> </p>
                <a href="<?php echo get_option('home') ?>/start/" class="startbtn"><img src="<?php echo get_template_directory_uri(); ?>/common/images/start.png" alt="startbtn"></a>
            </div>
        </section>



        <?php

    endwhile;
endif;
?>       


<?php get_footer(); ?>