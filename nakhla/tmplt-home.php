<?php
/**
 * Template Name: Home Template
 *
 */
get_header();
wp_enqueue_script('jquery');
add_action('wp_footer', 'cc_pagescipts', 21);

function cc_pagescipts() {
    
}
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



        <section class="slider">
            <img src="<?php echo get_template_directory_uri(); ?>/common/images/cover.jpg" alt="img">

        </section> 

        <section class="content menubar downmenu">
            <div class="container">
                <?php include('menu.php'); ?>
            </div>
        </section>



        <?php
    endwhile;
endif;
?>       


<?php get_footer(); ?>